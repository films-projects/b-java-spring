package nl.hkant.films.services.dto;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.external.ExtendedContentInfo;

public record FullContentInfo(
    Content content,
    ContentDetails<?> contentDetails,
    ExtendedContentInfo extendedContentInfo
) {

    public FullContentInfo(Content content) {
        this(content, null, null);
    }
}
