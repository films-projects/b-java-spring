package nl.hkant.films.services.util;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class NumberUtils {

    public static double roundToOneDecimal(double input) {
        return BigDecimal.valueOf(input).setScale(1, RoundingMode.HALF_EVEN).doubleValue();
    }
}
