package nl.hkant.films.services.util;

import jakarta.annotation.Nullable;
import org.apache.commons.lang3.StringUtils;

public class ImageUtil {

    public static final String BACKDROP_URL = "https://image.tmdb.org/t/p/original/";
    public static final String POSTER_URL = "https://image.tmdb.org/t/p/w300/";

    public static String backdropUrl(@Nullable String backdropPath) {
        if (backdropPath == null) {
            return null;
        }
        return BACKDROP_URL + StringUtils.removeStart(backdropPath, "/");
    }

    public static String posterUrl(@Nullable String posterPath) {
        if (posterPath == null) {
            return null;
        }
        return POSTER_URL + StringUtils.removeStart(posterPath, "/");
    }
}
