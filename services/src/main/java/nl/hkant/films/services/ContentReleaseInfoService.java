package nl.hkant.films.services;

import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.Series;
import nl.hkant.films.domain.SeriesStatus;
import nl.hkant.films.domain.User;
import nl.hkant.films.external.EpisodeInfo;
import nl.hkant.films.external.IEpisodeInfoService;
import nl.hkant.films.services.config.CachingConfig;
import nl.hkant.films.services.repositories.ContentRepository;
import nl.hkant.films.services.repositories.SeriesRepository;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Service
public class ContentReleaseInfoService {

    private final IEpisodeInfoService episodeInfoService;
    private final SeriesRepository seriesRepository;
    private final ContentRepository contentRepository;

    public ContentReleaseInfoService(
        IEpisodeInfoService iEpisodeInfoService,
        SeriesRepository seriesRepository,
        ContentRepository contentRepository
    ) {
        this.episodeInfoService = iEpisodeInfoService;
        this.seriesRepository = seriesRepository;
        this.contentRepository = contentRepository;
    }

    @Nonnull
    public List<SeriesAndEpisodes> getWatchingOrAnticipatedSeriesLastAndNextEpisodes(@Nonnull User user) {
        return seriesRepository.findByWatchingTrueOrAnticipatedTrueForUser(user.getId())
            .stream()
            .filter(it -> it.getSeriesDetails() != null)
            .filter(it ->
                it.getSeriesDetails().getStatus() == SeriesStatus.ONGOING ||
                it.getSeriesDetails().getStatus() == SeriesStatus.UPCOMING
            )
            .map(series -> {
                var lastAndNextEpisode = episodeInfoService.getLastAndUpcomingEpisodes(series.getSeriesDetails().getExternalContentId());
                return new SeriesAndEpisodes(
                    new ContentRelease(series),
                    lastAndNextEpisode.getLastEpisode(),
                    lastAndNextEpisode.getNextEpisode()
                );
            })
            .toList();
    }

    @Nonnull
    @Cacheable(cacheNames = CachingConfig.CacheKey.UPCOMING_MOVIES, key = "#user.getId() + '-' + #until.toString()")
    public List<ContentRelease> upcomingMovies(@Nonnull User user, @Nonnull LocalDate until) {
        return contentRepository.findWithReleaseDateBetween(user.getId(), LocalDate.now().plusDays(1), until, null)
            .stream()
            .map(ContentRelease::new)
            .toList();
    }

    @Nonnull
    @Cacheable(cacheNames = CachingConfig.CacheKey.RECENT_MOVIES, key = "#user.getId() + '-' + #from.toString()")
    public List<ContentRelease> recentMovies(@Nonnull User user, @Nonnull LocalDate from) {
        return contentRepository.findWithReleaseDateBetween(user.getId(), from, LocalDate.now(), false)
            .stream()
            .map(ContentRelease::new)
            .toList();
    }

    public record SeriesAndEpisodes(
        ContentRelease series,
        @Nullable EpisodeInfo lastEpisode,
        @Nullable EpisodeInfo nextEpisode
    ) {

        @JsonIgnore
        public Optional<String> getNextEpisodeAirDate() {
            return Optional.ofNullable(nextEpisode).map(e -> e.getAirDate().toString());
        }

        @JsonIgnore
        public Optional<String> getLastEpisodeAirDate() {
            return Optional.ofNullable(lastEpisode).map(e -> e.getAirDate().toString());
        }
    }

    public record ContentRelease(
        int id,
        String title,
        boolean specialInterest,
        String releaseDate,
        @Nullable String posterUrl,
        // series only
        String status
    ) implements Serializable {

        public ContentRelease(Content content) {
            this(
                content.getId(),
                content.getTitle(),
                content.isAnticipated(),
                Optional.ofNullable(content.getContentDetails().getAirDate()).map(Object::toString).orElse(null),
                content.getContentDetails().getPosterUrl().orElse(null),
                switch (content) {
                    case Series s -> s.getSeriesDetails().getStatus().getFriendlyName();
                    default -> null;
                }
            );
        }
    }
}

