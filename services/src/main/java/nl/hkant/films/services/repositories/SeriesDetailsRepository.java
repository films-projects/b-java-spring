package nl.hkant.films.services.repositories;

import nl.hkant.films.domain.SeriesDetails;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SeriesDetailsRepository extends CrudRepository<SeriesDetails, Integer> {
}
