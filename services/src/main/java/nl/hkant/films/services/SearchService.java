package nl.hkant.films.services;

import jakarta.annotation.Nonnull;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.SearchParams;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.domain.User;
import nl.hkant.films.services.repositories.ContentRepository;
import nl.hkant.films.services.repositories.FilmRepository;
import nl.hkant.films.services.repositories.SeriesRepository;
import nl.hkant.films.services.repositories.specifications.FilmSpecification;
import org.hibernate.Hibernate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.JpaSort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Component
public class SearchService {
    private final ContentRepository contentRepository;
    private final FilmRepository filmRepository;
    private final SeriesRepository seriesRepository;

    public SearchService(
        FilmRepository filmRepository,
        ContentRepository contentRepository,
        SeriesRepository seriesRepository
    ) {
        this.contentRepository = contentRepository;
        this.filmRepository = filmRepository;
        this.seriesRepository = seriesRepository;
    }

    @Transactional(readOnly = true)
    public Page<Content> searchReadOnly(@Nonnull User user, @Nonnull SearchParams params, @Nonnull Pageable page) {
        var spec = Specification
            .where(FilmSpecification.forUser(user))
            .and(FilmSpecification.hasTitleLike(params.getTitle()))
            .and(FilmSpecification.fromYear(params.getYearFrom()))
            .and(FilmSpecification.untilYear(params.getYearTo()))
            .and(FilmSpecification.isAnticipated(params.getAnticipated()))
            .and(FilmSpecification.myRating(params.getMyRating()))
            .and(FilmSpecification.seriesStatus(params.getType(), params.getStatus()))
            .and(FilmSpecification.watching(params.getType(), params.getWatching()))
            .and(FilmSpecification.minRating(params.getMinRating()))
            .and(FilmSpecification.watchOptions(params.getWatch()))
            .and(FilmSpecification.released(params.getType(), params.getReleased()));

        Pageable prop;
        if (!page.getSort().isSorted()) {
            prop = PageRequest.of(page.getPageNumber(), page.getPageSize(), JpaSort.by(Sort.Direction.ASC, "title"));
        } else {
            prop = page.getSort().get().findFirst().map(sort -> {
                    String prop222 = sort.getProperty().equals("rating")
                        ? "contentDetails_imdbRating"
                        : sort.getProperty();
                    return (Pageable) PageRequest.of(page.getPageNumber(), page.getPageSize(), JpaSort.by(sort.getDirection(), prop222));
                })
                .orElse(page);
        }
        if ("series".equals(params.getType())) {
            return seriesRepository.findAll(spec, prop);
        } else if ("film".equals(params.getType())) {
            return filmRepository.findAll(spec, prop);
        } else {
            return contentRepository.findAll(spec, prop);
        }
    }

    @Transactional(readOnly = true)
    public Optional<Content> getSuggestionReadOnly(@Nonnull User user, Boolean seen, Boolean anticipated, Double minRating, List<StreamingPlatform> watchOptions) {
        var film = contentRepository.getRandomMovieReleasedBefore(user.getId(), seen, anticipated, minRating, watchOptions, LocalDate.now());
        film.ifPresent(f -> Hibernate.initialize(f.getContentDetails()));
        return film;
    }
}
