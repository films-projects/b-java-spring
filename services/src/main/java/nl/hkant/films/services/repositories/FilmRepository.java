package nl.hkant.films.services.repositories;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.Film;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FilmRepository extends CrudRepository<Film, Integer>, JpaSpecificationExecutor<Content> {
}
