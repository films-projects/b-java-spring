package nl.hkant.films.services.scheduled;

import nl.hkant.films.domain.ConfigKey;
import nl.hkant.films.services.ConfigurationService;
import nl.hkant.films.services.ContentInfoService;
import nl.hkant.films.services.repositories.ContentDetailsRepository;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

@Service
@Profile("!dev")
public class ScheduledContentRefreshService {

    private final ContentInfoService contentInfoService;
    private final ContentDetailsRepository contentDetailsRepository;

    private final int daysBefore;
    private static final Logger log = LoggerFactory.getLogger(ScheduledContentRefreshService.class);

    public ScheduledContentRefreshService(
        ContentInfoService contentInfoService,
        ContentDetailsRepository contentDetailsRepository,
        ConfigurationService configurationService
    ) {
        this.contentInfoService = contentInfoService;
        this.contentDetailsRepository = contentDetailsRepository;
        daysBefore = Math.max(
            Integer.parseInt(configurationService.get(ConfigKey.REFRESH_EXPIRATION_LIMIT, "0")),
            0
        );
    }

    @Scheduled(fixedDelay = 5 * DateUtils.MILLIS_PER_MINUTE)
    public void refreshMovie() throws InterruptedException {
        var limit = LocalDate.now().minusDays(daysBefore);
        var details = contentDetailsRepository.findTop1ByLastRefreshNullOrLastRefreshBeforeOrderByLastRefresh(limit);
        if (details.isEmpty()) {
            log.info("Found no content to refresh with date before {}, sleeping {} day(s)", limit, daysBefore + 1);
            Thread.sleep(TimeUnit.DAYS.toMillis(daysBefore + 1));
        } else {
            log.info(
                "Refreshing content details {}, last refresh was {}, date limit was {}",
                details.get().getId(),
                details.get().getLastRefresh(),
                limit
            );
            contentInfoService.refreshDangerous(details.get().getId());
        }
    }
}
