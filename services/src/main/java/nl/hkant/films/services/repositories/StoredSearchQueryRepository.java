package nl.hkant.films.services.repositories;

import jakarta.transaction.Transactional;
import nl.hkant.films.domain.StoredSearchQuery;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface StoredSearchQueryRepository extends CrudRepository<StoredSearchQuery, Integer> {

    List<StoredSearchQuery> findByUserId(int userId);

    @Transactional
    void deleteByUserIdAndQuery(int userId, String query);
}
