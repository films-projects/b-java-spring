package nl.hkant.films.services.support;

import nl.hkant.films.domain.Film;
import nl.hkant.films.domain.Role;
import nl.hkant.films.domain.Series;
import nl.hkant.films.domain.User;
import nl.hkant.films.domain.UserRole;
import nl.hkant.films.services.ContentService;
import nl.hkant.films.services.UserService;
import nl.hkant.films.services.repositories.UserRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import jakarta.annotation.PostConstruct;

@Component
@Profile("dev")
public class DataBootstrapper {
	private final UserRepository userRepository;
	private final UserService userService;
	private final ContentService contentService;

	public DataBootstrapper(UserRepository userRepository, UserService userService, ContentService contentService) {
		this.userRepository = userRepository;
		this.userService = userService;
		this.contentService = contentService;
	}

	@PostConstruct
	public void init() {
		if (shouldRun()) {
			insertUsers();
			addFilms();
		}
	}

	private boolean shouldRun() {
		return userRepository.count() == 0;
	}

	private void insertUsers() {
		User henk = new User("henk", "kant", true);
		henk.addUserRole(new UserRole(Role.ROLE_ADMIN));
		henk.addUserRole(new UserRole(Role.ROLE_USER));
		userService.add(henk, henk.getPassword());
		User viewer = new User("viewer", "viewer", true);
		viewer.addUserRole(new UserRole(Role.ROLE_USER));
		userService.add(viewer, viewer.getPassword());
	}

	private void addFilms() {
		User user = userRepository.findByUsername("henk").orElseThrow();
		Film redsparrow = new Film();
		redsparrow.setTitle("Red Sparrow");
		redsparrow.setYear(2010);
		contentService.add(user, redsparrow);

		Film _5050 = new Film();
		_5050.setTitle("50/50");
		_5050.setYear(2010);
		contentService.add(user, _5050);

		Film cell211 = new Film();
		cell211.setTitle("Cell 211");
		cell211.setYear(2010);
		contentService.add(user, cell211);

		Series the100 = new Series();
		the100.setTitle("The 100");
		the100.setYear(2016);
		contentService.add(user, the100);
	}
}
