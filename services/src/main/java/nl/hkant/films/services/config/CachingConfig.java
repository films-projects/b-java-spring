package nl.hkant.films.services.config;

import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.jcache.JCacheCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.cache.CacheManager;
import javax.cache.Caching;
import javax.cache.configuration.MutableConfiguration;
import javax.cache.expiry.CreatedExpiryPolicy;
import javax.cache.expiry.Duration;
import java.util.concurrent.TimeUnit;

@Configuration
@EnableCaching
public class CachingConfig {


    @Bean
    public JCacheCacheManager jCacheCacheManager() {
        return new JCacheCacheManager(cacheManager());
    }

    @Bean
    public CacheManager cacheManager() {
        CacheManager cacheManager = Caching.getCachingProvider().getCacheManager();
        MutableConfiguration<Object, Object> ttlOneWeek =
                new MutableConfiguration<>()
                        .setTypes(Object.class, Object.class)
                        .setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new Duration(TimeUnit.DAYS, 7)));
        MutableConfiguration<Object, Object> ttlOneDay =
                new MutableConfiguration<>()
                        .setTypes(Object.class, Object.class)
                        .setExpiryPolicyFactory(CreatedExpiryPolicy.factoryOf(new Duration(TimeUnit.DAYS, 1)))
                        .setStoreByValue(true);
        cacheManager.createCache(CacheKey.WATCH_PROVIDERS, ttlOneWeek);
        cacheManager.createCache(CacheKey.EPISODE_INFO, ttlOneDay);
        cacheManager.createCache(CacheKey.RECENT_MOVIES, ttlOneDay);
        cacheManager.createCache(CacheKey.UPCOMING_MOVIES, ttlOneDay);
        return cacheManager;
    }

    public class CacheKey {

        public static final String WATCH_PROVIDERS = "watchProviders";
        public static final String EPISODE_INFO = "episodeInfo";
        public static final String RECENT_MOVIES = "movies.recent";
        public static final String UPCOMING_MOVIES = "movies.upcoming";
    }
}
