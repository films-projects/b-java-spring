package nl.hkant.films.services;

import jakarta.annotation.Nonnull;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.Series;
import nl.hkant.films.domain.User;
import nl.hkant.films.domain.exceptions.ContentNotFoundException;
import nl.hkant.films.external.ContentType;
import nl.hkant.films.services.external.ExternalContentService;
import nl.hkant.films.services.repositories.ContentRepository;
import nl.hkant.films.services.validators.ContentValidator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
public class ContentService {

    private final ContentValidator contentValidator;
    private final ContentRepository contentRepository;
    private final ExternalContentService externalContentService;

    public ContentService(
        ContentValidator contentValidator,
        ContentRepository contentRepository,
        ExternalContentService externalContentService
    ) {
        this.contentValidator = contentValidator;
        this.contentRepository = contentRepository;
        this.externalContentService = externalContentService;
    }

    @Transactional
    public Content findOrException(@Nonnull User user, int id) {
        return contentRepository.findByIdAndUserId(id, user.getId())
            .orElseThrow(() -> new ContentNotFoundException(id));
    }

    @Transactional
    public Content add(@Nonnull User user, @Nonnull Content content) {
        contentValidator.validate(content);
        content.setUserId(user.getId());
        contentRepository.save(content);
        return content;
    }

    @Transactional
    public void delete(@Nonnull User user, int id) {
        var content = findOrException(user, id);
        contentRepository.delete(content);
    }

    @Transactional
    public void update(
        @Nonnull User user,
        int id,
        Optional<String> title,
        Optional<Integer> year,
        Optional<Integer> myRating,
        Optional<Boolean> watching,
        Optional<Boolean> specialInterest
    ) {
        var content = findOrException(user, id);
        title.ifPresent(content::setTitle);
        year.ifPresent(content::setYear);
        myRating.ifPresent(newRating -> {
            content.setRating(newRating);
            content.setSeen(newRating != 0);
            if (newRating != 0) {
                content.setAnticipated(false);
            }
        });
        watching.ifPresent(it -> {
            if (content instanceof Series series) {
                series.setWatching(it);
                if (it) {
                    series.setAnticipated(false);
                }
            }
        });
        specialInterest.ifPresent(it -> {
            if (content instanceof Series && ((Series) content).isWatching()) {
                return;
            }

            if (!content.isSeen() && content.getRating() == 0) {
                content.setAnticipated(it);
                contentRepository.save(content);
            }
        });
        contentValidator.validate(content);
        contentRepository.save(content);
    }

    @Transactional
    public Optional<Content> createFromExternalInfo(
        @Nonnull User user,
        int externalContentId,
        boolean interested,
        ContentType contentType
    ) {
        return externalContentService.constructCompleteContent(externalContentId, contentType)
            .map(content -> {
                content.setUserId(user.getId());
                content.setAnticipated(interested);
                return contentRepository.save(content);
            });
    }
}
