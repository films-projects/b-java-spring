package nl.hkant.films.services.util;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.domain.Film;
import nl.hkant.films.domain.MovieDetails;
import nl.hkant.films.domain.Series;
import nl.hkant.films.domain.SeriesDetails;
import nl.hkant.films.domain.SeriesStatus;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.external.StreamingProvider;
import nl.hkant.films.external.results.FilmResult;
import nl.hkant.films.external.results.IContentResult;
import nl.hkant.films.external.results.SeriesResult;

public class ExternalContentConverter {

    public static Content convertToContent(IContentResult contentResult) {
        return switch (contentResult) {
            case FilmResult filmResult -> convertToFilm(filmResult);
            case SeriesResult seriesResult -> convertToSeries(seriesResult);
            default -> throw new IllegalStateException("Unexpected value: " + contentResult);
        };
    }

    private static Film convertToFilm(FilmResult filmResult) {
        var film = new Film();
        film.setTitle(filmResult.getTitle());
        film.setYear(filmResult.getYear());
        film.setContentDetails(convertToMovieDetails(filmResult));
        return film;
    }

    public static ContentDetails<?> convertToContentDetails(IContentResult contentResult) {
        return switch (contentResult) {
            case FilmResult filmResult -> convertToMovieDetails(filmResult);
            case SeriesResult seriesResult -> convertToSeriesDetails(seriesResult);
            default -> throw new IllegalStateException("Unexpected value: " + contentResult);
        };
    }

    public static MovieDetails convertToMovieDetails(FilmResult filmResult) {
        var details = new MovieDetails();
        details.setExternalContentId(filmResult.getExternalContentId());
        details.setPosterPath(filmResult.getPosterPath());
        details.setOverview(filmResult.getOverview());
        details.setImdbId(filmResult.getImdbId());
        details.setRuntime(filmResult.getRuntime());
        details.setBackdropPath(filmResult.getBackdropPath());
        details.setReleaseDate(filmResult.getAirDate());
        details.setExternalContentTitle(filmResult.getTitle());
        return details;
    }

    private static Series convertToSeries(SeriesResult seriesResult) {
        var series = new Series();
        series.setTitle(seriesResult.getTitle());
        series.setYear(seriesResult.getYear());
        series.setContentDetails(convertToSeriesDetails(seriesResult));
        return series;
    }

    public static SeriesDetails convertToSeriesDetails(SeriesResult info) {
        var details = new SeriesDetails();
        details.setExternalContentId(info.getExternalContentId());
        details.setPosterPath(info.getPosterPath());
        details.setOverview(info.getOverview());
        details.setBackdropPath(info.getBackdropPath());
        details.setNumEpisodes(info.getNumEpisodes());
        details.setNumSeasons(info.getNumSeasons());
        details.setStatus(fromExternalStatus(info.getStatus()));
        details.setImdbId(info.getImdbId());

        details.setFirstAirDate(info.getFirstAirDate());
        details.setLastAirDate(info.getLastAirDate());
        details.setEpisodeRuntime(info.getEpisodeRuntime());

        details.setExternalContentTitle(info.getTitle());
        return details;
    }

    private static SeriesStatus fromExternalStatus(nl.hkant.films.external.SeriesStatus externalStatus) {
        return switch (externalStatus) {
            case ONGOING -> SeriesStatus.ONGOING;
            case ENDED -> SeriesStatus.ENDED;
            case CANCELLED -> SeriesStatus.CANCELLED;
            case UPCOMING -> SeriesStatus.UPCOMING;
            case UNKNOWN -> SeriesStatus.UNKNOWN;
        };
    }

    public static StreamingPlatform fromExternalStreamingProvider(StreamingProvider streamingProvider) {
        return switch (streamingProvider) {
            case AMAZON_PRIME -> StreamingPlatform.AMAZON_PRIME;
            case APPLE_TV -> StreamingPlatform.APPLE_TV;
            case HBO_MAX -> StreamingPlatform.HBO_MAX;
            case NETFLIX -> StreamingPlatform.NETFLIX;
            case SKY_SHOWTIME -> StreamingPlatform.SKY_SHOWTIME;
        };
    }
}
