package nl.hkant.films.services;

import nl.hkant.films.services.repositories.ContentDetailsRepository;
import nl.hkant.films.services.util.ImageUtil;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.Random;

@Service
public class BackgroundImageService {

    private final ContentDetailsRepository contentDetailsRepository;

    public BackgroundImageService(ContentDetailsRepository contentDetailsRepository) {
        this.contentDetailsRepository = contentDetailsRepository;
    }

    private final List<String> filenames = List.of(
        "1.jpeg",
        // "2.jpg", // used for login
        "3.png",
        "4.jpg",
        "5.png",
        "6.jpg",
        "7.png",
        "8.jpg",
        "9.jpg",
        "10.jpg",
        "11.jpg",
        "12.jpg",
        "13.jpg",
        "14.jpg",
        "15.jpg",
        "16.jpg",
        "17.jpg",
        "18.jpg"
    );

    public Optional<String> getContentBackgroundImage() {
        return contentDetailsRepository.getRandomBackdropPath()
            .map(ImageUtil::backdropUrl);
    }

    public String getRandomBackgroundImage() {
        int size = filenames.size();
        int item = new Random().nextInt(size);
        return "/img/background/" + filenames.get(item);
    }
}
