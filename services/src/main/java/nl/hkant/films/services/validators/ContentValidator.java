package nl.hkant.films.services.validators;

import nl.hkant.films.domain.Content;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;

import java.time.LocalDateTime;

@Component
public class ContentValidator extends AbstractValidator<Content> {

	@Override
	public void validate(Content object, Errors errors) {
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "title", "error.validation.film.title.empty");
		LocalDateTime d = LocalDateTime.now();
		if (object.getYear() != 0 && object.getYear() < 1900) {
			errors.rejectValue("year", "error.validation.film.year.tooLow");
		}
		if (object.getYear() > d.getYear() + 3) {
			errors.rejectValue("year", "error.validation.film.year.tooHigh");
		}
		if (object.isSeen() && object.getRating() == 0) {
			errors.rejectValue("seen", "error.validation.film.seen.noRating");
		}
		if (object.getRating() != 0 && !object.isSeen()) {
			errors.rejectValue("rating", "error.validation.film.rating.notSeen");
		}
		if (object.isAnticipated() && object.getRating() != 0) {
			errors.rejectValue("rating", "error.validation.film.anticipated.hasRating");
		}
	}
}
