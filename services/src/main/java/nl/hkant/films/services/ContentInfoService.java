package nl.hkant.films.services;

import jakarta.annotation.Nonnull;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.domain.MovieDetails;
import nl.hkant.films.domain.SeriesDetails;
import nl.hkant.films.domain.User;
import nl.hkant.films.external.ContentType;
import nl.hkant.films.external.IExternalContentInfoFetchService;
import nl.hkant.films.external.results.FilmResult;
import nl.hkant.films.external.results.SeriesResult;
import nl.hkant.films.services.dto.FullContentInfo;
import nl.hkant.films.services.external.ExternalContentService;
import nl.hkant.films.services.repositories.ContentDetailsRepository;
import nl.hkant.films.services.util.ExternalContentConverter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class ContentInfoService {

    private final Logger logger = LoggerFactory.getLogger(ContentInfoService.class);

    private final ContentService contentService;
    private final ContentDetailsRepository contentDetailsRepository;
    private final IExternalContentInfoFetchService externalContentInfoFetchService;
    private final ExternalContentService externalContentService;

    public ContentInfoService(
        ContentService contentService,
        ContentDetailsRepository contentDetailsRepository,
        IExternalContentInfoFetchService externalContentInfoFetchService,
        ExternalContentService externalContentService
    ) {
        this.contentService = contentService;
        this.contentDetailsRepository = contentDetailsRepository;
        this.externalContentInfoFetchService = externalContentInfoFetchService;
        this.externalContentService = externalContentService;
    }

    private Optional<? extends ContentDetails<?>> getDetails(@Nonnull String id, @Nonnull ContentType type) {
        if (id.startsWith("tt")) {
            Optional<ContentDetails<?>> externalResult = Optional
                    .ofNullable(externalContentInfoFetchService.findByImdbId(id, type))
                    .map(ExternalContentConverter::convertToContentDetails);
            if (externalResult.isPresent()) {
                int externalContentId = externalResult.get().getExternalContentId();

                Optional<? extends ContentDetails<?>> existingDetails = switch (type) {
                    case FILM -> contentDetailsRepository.findTop1MovieDetailsByExternalContentIdOrderById(externalContentId);
                    case SERIES -> contentDetailsRepository.findTop1SeriesDetailsByExternalContentIdOrderById(externalContentId);
                };
                if (existingDetails.isPresent()) {
                    return existingDetails;
                }
            }
            return externalResult;
        } else {
            var externalContentId = Integer.parseInt(id);
            Optional<? extends ContentDetails<?>> existingDetails = switch (type) {
                case FILM -> contentDetailsRepository.findTop1MovieDetailsByExternalContentIdOrderById(externalContentId);
                case SERIES -> contentDetailsRepository.findTop1SeriesDetailsByExternalContentIdOrderById(externalContentId);
            };
            if (existingDetails.isPresent()) {
                return existingDetails;
            }
            return Optional.ofNullable(externalContentInfoFetchService.findByExternalContentId(externalContentId, type))
                    .map(ExternalContentConverter::convertToContentDetails);
        }
    }

    @Transactional
    public Content storeInfoForContent(User user, int contentId, @Nonnull String id) {
        var content = contentService.findOrException(user, contentId);
        getDetails(id, content.contentType()).ifPresent(contentDetails -> {
            externalContentService.setImdbRating(contentDetails);
            externalContentService.setWatchOptions(contentDetails);
            content.setContentDetails(contentDetails);
        });
        return content;
    }

    @Transactional
    public Content refresh(User user, int id) {
        var content = contentService.findOrException(user, id);
        refreshInner(content);
        return content;
    }

    @Transactional
    public void refreshDangerous(int id) {
        var details = contentDetailsRepository.findById(id)
            .orElseThrow(() -> new RuntimeException("Could not find details by id " + id));
        switch (details) {
            case MovieDetails movieDetails -> refreshMovie(movieDetails);
            case SeriesDetails seriesDetails -> refreshSeries(seriesDetails);
            default -> {
            }
        }
    }

    private void refreshInner(@Nonnull Content content) {
        Optional.ofNullable(content.getContentDetails()).ifPresent(it -> {
            if (it instanceof MovieDetails d) {
                refreshMovie(d);
            } else if (it instanceof SeriesDetails d) {
                refreshSeries(d);
            }
        });
    }

    private void refreshMovie(MovieDetails details) {
        contentDetailsRepository.findMovieDetailsById(details.getId()).ifPresent(d -> {
            try {
                var newDetails = Optional.ofNullable(
                    externalContentInfoFetchService.findByExternalContentId(d.getExternalContentId(), ContentType.FILM)
                )
                    .map(FilmResult.class::cast)
                    .map(ExternalContentConverter::convertToMovieDetails)
                    .orElseThrow();

                externalContentService.setImdbRating(newDetails);
                externalContentService.setWatchOptions(newDetails);
                d.update(newDetails);
                d.updateLastRefresh();
                contentDetailsRepository.save(d);
            } catch (Exception e) {
                logger.error("Failed to refresh movie details {}, deleting it...", details.getId(), e);
                for (Content content : d.getContent()) {
                    content.setContentDetails(null);
                }
                contentDetailsRepository.delete(details);
            }
        });
    }

    private void refreshSeries(SeriesDetails details) {
        contentDetailsRepository.findSeriesDetailsById(details.getId()).ifPresent(d -> {
            try {
                var newDetails = Optional.ofNullable(
                        externalContentInfoFetchService.findByExternalContentId(d.getExternalContentId(), ContentType.SERIES)
                    )
                    .map(SeriesResult.class::cast)
                    .map(ExternalContentConverter::convertToSeriesDetails)
                    .orElseThrow();

                externalContentService.setImdbRating(newDetails);
                externalContentService.setWatchOptions(newDetails);
                d.update(newDetails);
                d.updateLastRefresh();
                contentDetailsRepository.save(d);
            } catch (Exception e) {
                logger.error("Failed to refresh series details {}, deleting it...", details.getId(), e);
                for (Content content : d.getContent()) {
                    content.setContentDetails(null);
                }
                contentDetailsRepository.delete(details);
            }
        });
    }

    @Transactional
    public Content disconnect(User user, int contentId) {
        var content = contentService.findOrException(user, contentId);
        var details = content.getContentDetails();
        content.setContentDetails(null);
        details.getContent().remove(content);
        if (details.getContent().isEmpty()) {
            contentDetailsRepository.delete(details);
        }
        return content;
    }

    public List<? extends ContentDetails<?>> searchByTypeAndName(ContentType type, String name) {
        var results = switch (type) {
            case ContentType.FILM -> externalContentInfoFetchService.searchMovie(name);
            case ContentType.SERIES -> externalContentInfoFetchService.searchSeries(name);
        };
        return results
            .stream()
            .map(ExternalContentConverter::convertToContentDetails)
            .toList();
    }

    @Transactional
    public FullContentInfo getFullContentInfo(User user, int contentId) {
        var content = contentService.findOrException(user, contentId);
        var contentType = content.contentType();
        return new FullContentInfo(
            content,
            content.getContentDetails(),
            Optional.ofNullable(content.getContentDetails())
                .map(cd ->
                    externalContentInfoFetchService.getAdditionalContentInfo(cd.getExternalContentId(), contentType)
                )
                .orElse(null)
        );
    }
}
