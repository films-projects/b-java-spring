package nl.hkant.films.services.external;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.external.ContentType;
import nl.hkant.films.external.IExternalContentInfoFetchService;
import nl.hkant.films.external.IImdbRatingService;
import nl.hkant.films.external.IStreamingProviderService;
import nl.hkant.films.services.repositories.ContentDetailsRepository;
import nl.hkant.films.services.util.ExternalContentConverter;
import org.springframework.stereotype.Service;

import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ExternalContentService {

    private final IImdbRatingService imdbRatingService;
    private final IExternalContentInfoFetchService externalContentInfoFetchService;
    private final IStreamingProviderService streamingProviderService;
    private final ContentDetailsRepository contentDetailsRepository;

    public ExternalContentService(
        IImdbRatingService imdbRatingService,
        IExternalContentInfoFetchService externalContentInfoFetchService,
        IStreamingProviderService streamingProviderService,
        ContentDetailsRepository contentDetailsRepository
    ) {
        this.imdbRatingService = imdbRatingService;
        this.externalContentInfoFetchService = externalContentInfoFetchService;
        this.streamingProviderService = streamingProviderService;
        this.contentDetailsRepository = contentDetailsRepository;
    }

    private Optional<? extends ContentDetails<?>> getExistingDetails(int externalContentId, ContentType contentType) {
        return switch (contentType) {
            case FILM -> contentDetailsRepository.findTop1MovieDetailsByExternalContentIdOrderById(externalContentId);
            case SERIES -> contentDetailsRepository.findTop1SeriesDetailsByExternalContentIdOrderById(externalContentId);
        };
    }

    public Optional<Content> constructCompleteContent(int externalContentId, ContentType contentType) {
        return Optional.ofNullable(
                externalContentInfoFetchService.findByExternalContentId(externalContentId, contentType)
            )
            .map(searchResult -> {
                var content = ExternalContentConverter.convertToContent(searchResult);
                var existingDetails = getExistingDetails(externalContentId, contentType);
                if (existingDetails.isPresent()) {
                    content.setContentDetails(existingDetails.get());
                } else {
                    setImdbRating(content.getContentDetails());
                    setWatchOptions(content.getContentDetails());
                }
                return content;
        });
    }

    public void setImdbRating(ContentDetails<?> contentDetails) {
        Optional.ofNullable(contentDetails.getImdbId()).ifPresent(imdbId ->
            contentDetails.setImdbRating(imdbRatingService.getImdbRating(imdbId))
        );
    }

    public void setWatchOptions(ContentDetails<?> contentDetails) {
        ContentType contentType = contentDetails.contentType();
        contentDetails.setWatchOptions(
            streamingProviderService.getStreamingProviders(contentDetails.getExternalContentId(), contentType)
                .stream()
                .map(ExternalContentConverter::fromExternalStreamingProvider)
                .collect(Collectors.toSet())
        );
    }
}
