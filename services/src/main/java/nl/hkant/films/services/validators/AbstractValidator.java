package nl.hkant.films.services.validators;

import nl.hkant.films.domain.exceptions.ValidationException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.springframework.validation.Errors;

public abstract class AbstractValidator<T> {

	public void validate(T object) {
		BeanPropertyBindingResult errors = new BeanPropertyBindingResult(object, object.getClass().toString());
		validate(object, errors);
		checkAndThrowErrors(errors);
	}

	public abstract void validate(T object, Errors errors);

	protected static void checkAndThrowErrors(Errors errors) {
		if (errors.hasErrors()) {
			throw new ValidationException();
		}
	}
}
