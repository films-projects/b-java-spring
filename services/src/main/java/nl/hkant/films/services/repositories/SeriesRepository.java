package nl.hkant.films.services.repositories;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.Series;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SeriesRepository extends CrudRepository<Series, Integer>, JpaSpecificationExecutor<Content> {
	@Query("select s from Series s where s.userId = ?1 and (s.anticipated = true or s.watching = true)")
	List<Series> findByWatchingTrueOrAnticipatedTrueForUser(int userId);
}
