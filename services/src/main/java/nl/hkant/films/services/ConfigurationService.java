package nl.hkant.films.services;

import nl.hkant.films.domain.ConfigKey;
import nl.hkant.films.domain.Configuration;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.services.repositories.ConfigurationRepository;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ConfigurationService {

    private final ConfigurationRepository configurationRepository;

    public ConfigurationService(ConfigurationRepository configurationRepository) {
        this.configurationRepository = configurationRepository;
    }

    public String getOrException(String configKey) {
        return configurationRepository.findById(configKey).map(Configuration::getConfigValue).orElseThrow();
    }

    public String get(String configKey, String orElse) {
        return configurationRepository.findById(configKey).map(Configuration::getConfigValue).orElse(orElse);
    }

    public List<StreamingPlatform> getEnabledWatchOptions() {
        try {
            var value = getOrException(ConfigKey.USABLE_WATCH_OPTIONS);
            return Arrays.stream(value.split(","))
                .map(StreamingPlatform::valueOf)
                .toList();
        } catch (Exception e) {
            return Collections.emptyList();
        }
    }
}
