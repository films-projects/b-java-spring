package nl.hkant.films.services.repositories.specifications;

import jakarta.annotation.Nonnull;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.SeriesStatus;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.domain.User;
import org.springframework.data.jpa.domain.Specification;

import java.time.LocalDate;
import java.util.List;

public class FilmSpecification {

    public static Specification<Content> trueOrNull(final boolean check, Specification<Content> result) {
        return check ? result : null;
    }

    public static Specification<Content> forUser(@Nonnull final User user) {
        return (root, query, cb) -> cb.equal(root.get("userId"), user.getId());
    }

    public static Specification<Content> hasTitleLike(final String title) {
        return trueOrNull(title != null && !title.isBlank(),
            (root, query, cb) -> cb.like(cb.lower(root.get("title")), "%" + title.toLowerCase() + "%"));
    }

    public static Specification<Content> fromYear(final Integer year) {
        return trueOrNull(year != null && year != 0, (root, query, cb) -> cb.ge(root.get("year"), year));
    }

    public static Specification<Content> untilYear(final Integer year) {
        return trueOrNull(year != null && year != 0, (root, query, cb) -> cb.le(root.get("year"), year));
    }

    public static Specification<Content> isAnticipated(final Boolean anticipated) {
        return trueOrNull(anticipated != null, (root, query, cb) -> cb.equal(root.get("anticipated"), anticipated));
    }

    public static Specification<Content> myRating(final List<Integer> rating) {
        return trueOrNull(rating != null, (root, query, cb) -> root.get("rating").in(rating));
    }

    public static Specification<Content> watching(String type, final Boolean watching) {
        return trueOrNull(watching != null && "series".equals(type),
            (root, query, cb) -> cb.equal(root.get("watching"), watching));
    }

    public static Specification<Content> released(String type, final Boolean released) {
        if (released == null || !"film".equals(type)) {
            return null;
        }
        if (released) {
            return (root, query, cb) -> cb.lessThanOrEqualTo(root.get("contentDetails").get("releaseDate"), LocalDate.now());
        } else {
            return (root, query, cb) -> cb.or(

                cb.isNull(root.get("contentDetails").get("releaseDate")),
                cb.greaterThan(root.get("contentDetails").get("releaseDate"), LocalDate.now())
            );
        }
    }

    public static Specification<Content> minRating(final Double minRating) {
        return trueOrNull(minRating != null, (root, query, cb) -> cb.ge(root.get("contentDetails").get("imdbRating"), minRating));
    }

    public static Specification<Content> watchOptions(final List<StreamingPlatform> options) {
        return trueOrNull(
            options != null && !options.isEmpty(),
            (root, query, cb) -> {
                var contentDetailsJoin = root.join("contentDetails");
                var optionsJoin = contentDetailsJoin.join("watchOptions");
                return optionsJoin.in(options);
            }
        );
    }

    public static Specification<Content> seriesStatus(final String type, final SeriesStatus status) {
        return trueOrNull(
            "series".equals(type) && status != null,
            (root, query, cb) -> cb.equal(root.get("contentDetails").get("status"), status)
        );
    }
}
