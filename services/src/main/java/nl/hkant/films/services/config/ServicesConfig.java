package nl.hkant.films.services.config;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import nl.hkant.films.domain.ConfigKey;
import nl.hkant.films.domain.config.DomainConfig;
import nl.hkant.films.external.IImdbRatingService;
import nl.hkant.films.external.omdb.OmdbService;
import nl.hkant.films.services.ConfigurationService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.security.web.authentication.rememberme.JdbcTokenRepositoryImpl;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

import javax.sql.DataSource;

@Configuration
@EnableJpaRepositories("nl.hkant.films.services.repositories")
@ComponentScan("nl.hkant.films.services")
@Import(DomainConfig.class)
public class ServicesConfig {

    private final DataSource dataSource;
    private final ConfigurationService configurationService;

    public ServicesConfig(DataSource dataSource, ConfigurationService configurationService) {
        this.dataSource = dataSource;
        this.configurationService = configurationService;
    }

    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.registerModule(new Jdk8Module());
        objectMapper.registerModule(new JavaTimeModule());
        objectMapper.setSerializationInclusion(Include.NON_NULL);
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        return objectMapper;
    }

    @Bean
    public PersistentTokenRepository persistentTokenRepository() {
        JdbcTokenRepositoryImpl db = new JdbcTokenRepositoryImpl();
        db.setDataSource(dataSource);
        return db;
    }

    @Bean
    public IImdbRatingService imdbRatingService() {
        String omdbKey = configurationService.getOrException(ConfigKey.OMDB_API_TOKEN);
        return new OmdbService(omdbKey);
    }

    @Bean
    public nl.hkant.films.external.tmdb.TmdbService newTmdbService() {
        String tmdbKey = configurationService.getOrException(ConfigKey.TMDB_KEY);
        return new nl.hkant.films.external.tmdb.TmdbService(tmdbKey);
    }
}
