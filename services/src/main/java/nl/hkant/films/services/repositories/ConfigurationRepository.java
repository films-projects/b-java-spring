package nl.hkant.films.services.repositories;

import nl.hkant.films.domain.Configuration;
import org.springframework.data.repository.CrudRepository;

public interface ConfigurationRepository extends CrudRepository<Configuration, String> {
}
