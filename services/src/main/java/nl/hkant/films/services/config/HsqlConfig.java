package nl.hkant.films.services.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import jakarta.annotation.PostConstruct;

@Profile("hsql")
@Configuration
public class HsqlConfig {

	@Value("${spring.datasource.url}")
	private String datasourceUrl;

	@PostConstruct
	public void init() {
		try {
			org.hsqldb.util.DatabaseManagerSwing.main(new String[]{"--url", datasourceUrl, "--noexit"});
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println();
			System.out.println("IGNORE THE ABOVE STACKTRACE");
		}
	}
}
