package nl.hkant.films.services.dto;

import java.util.List;

public record ExtendedContentInfo(
    List<String> cast,
    List<String> genres,
    String originalLanguage
) {

}
