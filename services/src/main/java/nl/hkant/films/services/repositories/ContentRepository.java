package nl.hkant.films.services.repositories;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.StreamingPlatform;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

@Repository
public interface ContentRepository extends
    PagingAndSortingRepository<Content, Integer>,
    CrudRepository<Content, Integer>,
    JpaRepository<Content, Integer>,
    JpaSpecificationExecutor<Content> {

    Optional<Content> findByIdAndUserId(int id, int userId);

    @Query(
        """
        FROM Film f
        LEFT JOIN f.contentDetails.watchOptions op
        WHERE ((:#{#watchOptions == null} = true) OR op in (:watchOptions))
        AND (f.userId = :userId)
        AND (:seen is null or f.seen = :seen)
        AND (:anticipated is null or f.anticipated = :anticipated)
        AND (:minRating is null or (f.contentDetails.imdbRating is not null and f.contentDetails.imdbRating >= :minRating))
        AND f.contentDetails.releaseDate is not null
        AND f.contentDetails.releaseDate < :now
        ORDER BY rand()
        LIMIT 1
        """
    )
    // https://hibernate.atlassian.net/browse/HHH-15968
    Optional<Content> getRandomMovieReleasedBefore(
        @Param("userId") int userId,
        @Param("seen") Boolean seen,
        @Param("anticipated") Boolean anticipated,
        @Param("minRating") Double minRating,
        @Param("watchOptions") List<StreamingPlatform> watchOptions,
        @Param("now") LocalDate now
    );

    @Query(
        """
        from Film f LEFT JOIN f.contentDetails.watchOptions op
        where (:seen is null or f.seen = :seen)
        and (f.userId = :userId)
        and (f.contentDetails.releaseDate >= :from)
        and (f.contentDetails.releaseDate <= :until)
        order by f.contentDetails.releaseDate
        """
    )
    List<Content> findWithReleaseDateBetween(
        @Param("userId") int userId,
        @Param("from") LocalDate from,
        @Param("until") LocalDate until,
        @Param("seen") Boolean seen
    );

    @Query(
        """
        SELECT CASE WHEN count(f)> 0 THEN true ELSE false END
        FROM Film f
        WHERE f.userId = :userId AND f.contentDetails.externalContentId = :externalContentId
        """
    )
    boolean userHasAddedFilm(int userId, int externalContentId);

    @Query(
        """
        SELECT CASE WHEN count(f)> 0 THEN true ELSE false END
        FROM Series f
        WHERE f.userId = :userId AND f.contentDetails.externalContentId = :externalContentId
        """
    )
    boolean userHasAddedSeries(int userId, int externalContentId);
}
