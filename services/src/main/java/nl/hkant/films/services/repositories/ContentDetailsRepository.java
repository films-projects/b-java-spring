package nl.hkant.films.services.repositories;

import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.domain.MovieDetails;
import nl.hkant.films.domain.SeriesDetails;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface ContentDetailsRepository extends CrudRepository<ContentDetails<?>, Integer> {

    Optional<ContentDetails<?>> findTop1ByLastRefreshNullOrLastRefreshBeforeOrderByLastRefresh(LocalDate now);

    Optional<MovieDetails> findMovieDetailsById(int id);

    Optional<SeriesDetails> findSeriesDetailsById(int id);

    Optional<MovieDetails> findTop1MovieDetailsByExternalContentIdOrderById(int externalContentId);

    Optional<SeriesDetails> findTop1SeriesDetailsByExternalContentIdOrderById(int externalContentId);

    @Query(
        """
        SELECT cd.backdropPath
        FROM ContentDetails cd
        WHERE cd.backdropPath is not null
        ORDER BY rand()
        LIMIT 1
        """
    )
    Optional<String> getRandomBackdropPath();
}
