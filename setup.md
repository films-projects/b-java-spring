# List endpoints

To verify compatibility with the frontend, you can list all registered endpoints when starting the application. Use
profile `listendpoints` to do so, so you can check the registered endpoints against the list of endpoints listed by the
frontend [here](frontend/endpoints.md).

# Frontend

See [frontend/readme.md](frontend/readme.md#requirements) for additional requirements for building the frontend.
