package nl.hkant.films.web.dto;

import jakarta.annotation.Nonnull;
import lombok.Getter;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.MovieDetails;

@Getter
public final class MovieContentInfoDTO extends ContentInfoDTO {

    private final int runtime;

    public MovieContentInfoDTO(Content content, @Nonnull MovieDetails info) {
        super(content, info);
        runtime = info.getRuntime();
    }
}
