package nl.hkant.films.web.dto;

import jakarta.annotation.Nonnull;
import lombok.Getter;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.SeriesDetails;

@Getter
public final class SeriesContentInfoDTO extends ContentInfoDTO {

    private final int numSeasons;
    private final String status;
    private final int episodeRuntime;
    private final int numEpisodes;

    public SeriesContentInfoDTO(@Nonnull Content content, @Nonnull SeriesDetails info) {
        super(content, info);
        episodeRuntime = info.getEpisodeRuntime();
        numEpisodes = info.getNumEpisodes();
        numSeasons = info.getNumSeasons();
        status = info.getStatus().getFriendlyName();
    }
}
