package nl.hkant.films.web.dto;

import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.services.util.ImageUtil;

import java.util.Objects;
import java.util.Optional;

public record ContentInfoSearchResultDTO(
        int externalId,
        String title,
        String posterImage,
        String overview,
        String releaseDate,
        Boolean contentHasBeenAdded
) {

    public ContentInfoSearchResultDTO(ContentDetails<?> contentDetails, Boolean contentHasBeenAdded) {
        this(
                contentDetails.getExternalContentId(),
                contentDetails.getExternalContentTitle(),
                ImageUtil.posterUrl(contentDetails.getPosterPath()),
                contentDetails.getOverview(),
                Optional.ofNullable(contentDetails.getAirDate()).map(Objects::toString).orElse(null),
                contentHasBeenAdded
        );
    }
}
