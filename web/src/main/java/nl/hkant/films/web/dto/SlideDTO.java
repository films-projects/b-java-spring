package nl.hkant.films.web.dto;

import lombok.Data;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.MovieDetails;
import nl.hkant.films.domain.Series;
import nl.hkant.films.domain.SeriesDetails;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.services.util.ImageUtil;
import nl.hkant.films.services.util.NumberUtils;

import java.util.Collections;
import java.util.Set;

@Data
public class SlideDTO {

    private int id;
    private String type;
    private String title;
    private int year;
    private boolean seen;
    private int myRating;
    private boolean anticipated;
    private String imdbId;
    private int numSeasons;
    private String status;
    private int runtime;
    private int episodeRuntime;
    private int numEpisodes;
    private String overview;
    private Double imdbRating;
    private Set<StreamingPlatform> streamingPlatforms = Collections.emptySet();
    private String posterImage;
    private boolean watching;

    public SlideDTO(Content content) {
        id = content.getId();
        type = content.getClass().getSimpleName().toLowerCase();
        title = content.getTitle();
        year = content.getYear();
        seen = content.isSeen();
        myRating = content.getRating();
        anticipated = content.isAnticipated();
        if (content instanceof Series s) {
            watching = s.isWatching();
        }

        if (content.getContentDetails() != null) {
            posterImage = ImageUtil.posterUrl(content.getContentDetails().getPosterPath());
            imdbId = content.getContentDetails().getImdbId();
            streamingPlatforms = content.getContentDetails().getWatchOptions();

            if (content.getContentDetails() instanceof SeriesDetails info) {
                episodeRuntime = info.getEpisodeRuntime();
                numEpisodes = info.getNumEpisodes();
                numSeasons = info.getNumSeasons();
                status = info.getStatus().getFriendlyName();
            } else if (content.getContentDetails() instanceof MovieDetails info) {
                runtime = info.getRuntime();
            }
            overview = content.getContentDetails().getOverview();
            if (content.getContentDetails().getImdbRating() != null) {
                imdbRating = NumberUtils.roundToOneDecimal(content.getContentDetails().getImdbRating());
            }
        }
    }
}
