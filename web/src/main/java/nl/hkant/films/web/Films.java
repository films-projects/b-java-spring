package nl.hkant.films.web;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class Films {
	public static void main(String[] args) {
		SpringApplicationBuilder builder = new SpringApplicationBuilder(Films.class);
		builder.headless(false);
		builder.run(args);
	}
}
