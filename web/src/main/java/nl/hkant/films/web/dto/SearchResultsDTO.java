package nl.hkant.films.web.dto;

import org.springframework.data.domain.Page;

public record SearchResultsDTO(Long timestamp, Page<SlideDTO> results) {
}
