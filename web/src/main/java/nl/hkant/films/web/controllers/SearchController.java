package nl.hkant.films.web.controllers;

import nl.hkant.films.domain.SearchParams;
import nl.hkant.films.domain.StoredSearchQuery;
import nl.hkant.films.domain.User;
import nl.hkant.films.services.SearchService;
import nl.hkant.films.web.dto.QueryString;
import nl.hkant.films.web.dto.SavedQuery;
import nl.hkant.films.services.repositories.StoredSearchQueryRepository;
import nl.hkant.films.web.dto.SearchResultsDTO;
import nl.hkant.films.web.dto.SlideDTO;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/search")
public class SearchController {

    private final SearchService searchService;
    private final StoredSearchQueryRepository storedSearchQueryRepository;

    public SearchController(SearchService searchService, StoredSearchQueryRepository storedSearchQueryRepository) {
        this.searchService = searchService;
        this.storedSearchQueryRepository = storedSearchQueryRepository;
    }

    @GetMapping("/do")
    public SearchResultsDTO search(@AuthenticationPrincipal User user, SearchParams params, Pageable page) {
        long timestamp = System.currentTimeMillis();
        var result = searchService.searchReadOnly(user, params, page);
        var results = new PageImpl<>(
            result.getContent().stream().map(SlideDTO::new).toList(),
            result.getPageable(),
            result.getTotalElements()
        );
        return new SearchResultsDTO(timestamp, results);
    }

    @GetMapping("/favorites")
    public List<SavedQuery> favorites(@AuthenticationPrincipal User user) {
        var saved = storedSearchQueryRepository.findByUserId(user.getId());
        return saved.stream().map(s -> new SavedQuery(s.getQuery(), s.params())).toList();
    }

    @PostMapping("/favorites/add")
    public SavedQuery addFavorite(@AuthenticationPrincipal User user, @RequestBody QueryString query) {
        var savedQuery = new StoredSearchQuery();
        savedQuery.setUserId(user.getId());
        savedQuery.setQuery(query.query());
        storedSearchQueryRepository.save(savedQuery);
        return new SavedQuery(savedQuery.getQuery(), savedQuery.params());
    }

    @PostMapping("/favorites/remove")
    public List<SavedQuery> removeFavorite(@AuthenticationPrincipal User user, @RequestBody QueryString query) {
        storedSearchQueryRepository.deleteByUserIdAndQuery(user.getId(), query.query());
        return favorites(user);
    }
}
