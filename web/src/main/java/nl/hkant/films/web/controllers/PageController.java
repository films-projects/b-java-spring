package nl.hkant.films.web.controllers;

import nl.hkant.films.services.BackgroundImageService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import java.lang.management.ManagementFactory;
import java.util.Map;

@Controller
@RequestMapping("/")
public class PageController {

    private final BackgroundImageService backgroundImageService;

    public PageController(BackgroundImageService backgroundImageService) {
        this.backgroundImageService = backgroundImageService;
    }

    @GetMapping
    public String root() {
        return "redirect:/search";
    }

    @GetMapping("/login")
    public String login() {
        return "login";
    }

    @GetMapping("/search")
    public ModelAndView page() {
        String backgroundImage = backgroundImageService.getContentBackgroundImage()
            .orElseGet(backgroundImageService::getRandomBackgroundImage);
        ModelAndView mav = new ModelAndView("search");
        mav.addObject("background", backgroundImage);
        return mav;
    }

    @GetMapping("/upcoming")
    public ModelAndView upcoming() {
        String backgroundImage = backgroundImageService.getContentBackgroundImage()
            .orElseGet(backgroundImageService::getRandomBackgroundImage);
        ModelAndView mav = new ModelAndView("upcoming");
        mav.addObject("background", backgroundImage);
        return mav;
    }

    @GetMapping("/memory-status")
    @ResponseBody
    public Map<String, Long> getMemoryStatistics() {
        var mb = 1000 * 1000;
        var memoryBean = ManagementFactory.getMemoryMXBean();
        var xmx = memoryBean.getHeapMemoryUsage().getMax() / mb;
        var xms = memoryBean.getHeapMemoryUsage().getInit() / mb;
        var stats = Map.of(
            "heapSize", Runtime.getRuntime().totalMemory(),
            "maxMemory", Runtime.getRuntime().maxMemory(),
            "freeMemory", Runtime.getRuntime().freeMemory(),
            "xmx", xmx,
            "xms", xms
        );
        return stats;
    }
}
