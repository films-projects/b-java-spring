package nl.hkant.films.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;

@Configuration
@EnableMethodSecurity
public class SecurityConfig {

    private final PersistentTokenRepository persistentTokenRepository;
    private final PasswordEncoder passwordEncoder;
    private final UserDetailsService userDetailsService;

    public SecurityConfig(
        PersistentTokenRepository persistentTokenRepository,
        PasswordEncoder passwordEncoder,
        UserDetailsService userDetailsService
    ) {
        this.persistentTokenRepository = persistentTokenRepository;
        this.passwordEncoder = passwordEncoder;
        this.userDetailsService = userDetailsService;
    }

    @Bean
    public AuthenticationProvider authManager() {
        DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userDetailsService);
        provider.setPasswordEncoder(passwordEncoder);
        return provider;
    }

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
            .csrf(AbstractHttpConfigurer::disable)
            .authorizeHttpRequests(auth ->
                auth
                    .requestMatchers("/login", "/memory-status", "/error", "/actuator/**", "/css/**", "/js/**", "/favicon.ico", "/img/background/**")
                    .permitAll()
                    .requestMatchers("/**")
                    .authenticated()

            )
            .formLogin(login ->
                login.loginPage("/login")
                    .usernameParameter("username")
                    .passwordParameter("password")
            )
            .logout(logout ->
                logout.logoutSuccessUrl("/login?logout")
            )
            .exceptionHandling(ex ->
                ex.accessDeniedPage("/403")
            )
            .rememberMe(re ->
                re.rememberMeParameter("rememberme")
                    .tokenRepository(persistentTokenRepository)
                    .tokenValiditySeconds(2419200)
                    .userDetailsService(userDetailsService)
            );
        return http.build();
    }
}
