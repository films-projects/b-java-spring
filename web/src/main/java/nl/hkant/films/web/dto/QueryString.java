package nl.hkant.films.web.dto;

public record QueryString(String query) {
}
