package nl.hkant.films.web.controllers;

import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.services.ConfigurationService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/watch")
public class WatchController {

    private final ConfigurationService configurationService;

    public WatchController(
        ConfigurationService configurationService
    ) {
        this.configurationService = configurationService;
    }

    @GetMapping("/options")
    @ResponseBody
    public List<StreamingPlatform> usableWatchOptions() {
        return configurationService.getEnabledWatchOptions();
    }
}
