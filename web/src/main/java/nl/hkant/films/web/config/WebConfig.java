package nl.hkant.films.web.config;

import nl.hkant.films.services.config.ServicesConfig;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@ComponentScan("nl.hkant.films.domain")
@Import({ServicesConfig.class})
public class WebConfig {
}
