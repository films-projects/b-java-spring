package nl.hkant.films.web.controllers;

import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.Film;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.domain.User;
import nl.hkant.films.external.ContentType;
import nl.hkant.films.services.ContentInfoService;
import nl.hkant.films.services.ContentService;
import nl.hkant.films.services.SearchService;
import nl.hkant.films.services.dto.FullContentInfo;
import nl.hkant.films.web.dto.FullContentInfoDTO;
import nl.hkant.films.web.dto.NewContentDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/film")
public class FilmController {

    private final ContentService contentService;
    private final SearchService searchService;
    private final ContentInfoService contentInfoService;

    public FilmController(
        ContentService contentService,
        SearchService searchService,
        ContentInfoService contentInfoService
    ) {
        this.contentService = contentService;
        this.searchService = searchService;
        this.contentInfoService = contentInfoService;
    }

    @PostMapping("/new")
    public NewContentDTO newFilm(@AuthenticationPrincipal User user, @RequestBody Film film) {
        film.setSeen(film.getRating() != 0);
        contentService.add(user, film);
        return new NewContentDTO(film.getId(), film.getTitle());
    }

    @PostMapping("/newFromExternal/{externalContentId}")
    public NewContentDTO newFromExternal(
        @AuthenticationPrincipal User user,
        @PathVariable int externalContentId,
        @RequestParam(defaultValue = "false", required = false) boolean interested
    ) {
        return contentService.createFromExternalInfo(user, externalContentId, interested, ContentType.FILM)
            .map(film -> new NewContentDTO(film.getId(), film.getTitle()))
            .orElseThrow();
    }

    @GetMapping("/suggest")
    public FullContentInfoDTO search(
        @AuthenticationPrincipal User user,
        @RequestParam(required = false) Boolean seen,
        @RequestParam(required = false) Boolean anticipated,
        @RequestParam(required = false) Double minRating,
        @RequestParam(required = false) List<StreamingPlatform> watchOptions
    ) {
        var info = searchService.getSuggestionReadOnly(user, seen, anticipated, minRating, watchOptions)
            .map(Content::getId)
            .map(it -> contentInfoService.getFullContentInfo(user, it))
            .orElseGet(() -> new FullContentInfo(new Film()));
        return new FullContentInfoDTO(info);
    }
}
