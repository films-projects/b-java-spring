package nl.hkant.films.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.annotation.Profile;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
@Profile("listendpoints")
public class ListEndpointListener implements ApplicationListener<ContextRefreshedEvent> {

    private final Logger logger = LoggerFactory.getLogger(ListEndpointListener.class);

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        RequestMappingHandlerMapping requestMappingHandlerMapping = applicationContext
                .getBean("requestMappingHandlerMapping", RequestMappingHandlerMapping.class);
        Map<RequestMappingInfo, HandlerMethod> map = requestMappingHandlerMapping
                .getHandlerMethods();
        List<Map.Entry<RequestMappingInfo, HandlerMethod>> endpoints = new ArrayList<>(map.entrySet());
        endpoints.sort((o1, o2) -> {
            String pattern1 = o1.getKey().getPatternValues().stream().findFirst().orElse("");
            String pattern2 = o2.getKey().getPatternValues().stream().findFirst().orElse("");
            return pattern1.compareTo(pattern2);
        });
        endpoints.forEach(endpoint -> logger.info("{} {}", endpoint.getKey(), endpoint.getValue()));
    }
}
