package nl.hkant.films.web.dto;

import jakarta.annotation.Nonnull;
import jakarta.annotation.Nullable;
import lombok.Getter;
import nl.hkant.films.domain.Content;
import nl.hkant.films.domain.ContentDetails;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.services.util.ImageUtil;
import nl.hkant.films.services.util.NumberUtils;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

@Getter
public sealed class ContentInfoDTO permits SeriesContentInfoDTO, MovieContentInfoDTO {

    private final int id;
    private final Set<StreamingPlatform> streamingPlatforms;
    private String posterImage = null;
    private String imdbId = null;
    private String overview = null;
    private Double imdbRating = null;

    public ContentInfoDTO(@Nonnull Content content, @Nullable ContentDetails<?> info) {
        id = content.getId();
        this.streamingPlatforms = Optional.ofNullable(info)
            .map(ContentDetails::getWatchOptions)
            .orElse(Collections.emptySet());
        if (info != null) {
            posterImage = ImageUtil.posterUrl(info.getPosterPath());
            imdbId = info.getImdbId();
            overview = info.getOverview();
            if (content.getContentDetails().getImdbRating() != null) {
                imdbRating = NumberUtils.roundToOneDecimal(content.getContentDetails().getImdbRating());
            }
        }
    }
}
