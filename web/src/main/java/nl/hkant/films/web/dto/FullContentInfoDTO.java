package nl.hkant.films.web.dto;

import lombok.Getter;
import nl.hkant.films.domain.MovieDetails;
import nl.hkant.films.domain.SeriesDetails;
import nl.hkant.films.domain.StreamingPlatform;
import nl.hkant.films.services.dto.FullContentInfo;
import nl.hkant.films.services.util.ImageUtil;
import nl.hkant.films.services.util.NumberUtils;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Getter
public class FullContentInfoDTO {
    private final Integer id;
    private final String title;
    private final String type;
    private final int year;
    private final boolean seen;
    private final int myRating;
    private final boolean anticipated;
    private Set<StreamingPlatform> streamingPlatforms = Collections.emptySet();
    private String overview;
    private String imdbId;
    private Double imdbRating;
    private String posterImage;
    private int runtime;
    private String status;
    private int episodeRuntime;
    private int numEpisodes;
    private List<String> cast;
    private List<String> genres;
    private String originalLanguage;
    private LocalDate lastRefresh;

    public FullContentInfoDTO(FullContentInfo info) {
        type = info.content().getClass().getSimpleName().toLowerCase();

        id = info.content().getId();
        title = info.content().getTitle();
        year = info.content().getYear();
        seen = info.content().isSeen();
        myRating = info.content().getRating();
        anticipated = info.content().isAnticipated();
        if (info.contentDetails() != null) {
            streamingPlatforms = info.contentDetails().getWatchOptions();
            overview = info.contentDetails().getOverview();
            imdbId = info.contentDetails().getImdbId();
            imdbRating = NumberUtils.roundToOneDecimal(info.contentDetails().getImdbRating());
            posterImage = ImageUtil.posterUrl(info.contentDetails().getPosterPath());
            lastRefresh = info.contentDetails().getLastRefresh();
        }
        if (info.contentDetails() instanceof MovieDetails m) {
            runtime = m.getRuntime();
        } else if (info.contentDetails() instanceof SeriesDetails s) {
            status = s.getStatus().getFriendlyName();
            episodeRuntime = s.getEpisodeRuntime();
            numEpisodes = s.getNumEpisodes();
        }
        if (info.extendedContentInfo() != null) {
            cast = info.extendedContentInfo().getCast();
            genres = info.extendedContentInfo().getGenres();
            originalLanguage = info.extendedContentInfo().getOriginalLanguage();
        }
    }
}
