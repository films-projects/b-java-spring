package nl.hkant.films.web.controllers;

import nl.hkant.films.domain.Series;
import nl.hkant.films.domain.User;
import nl.hkant.films.external.ContentType;
import nl.hkant.films.external.EpisodeInfo;
import nl.hkant.films.external.IEpisodeInfoService;
import nl.hkant.films.external.LastAndNextEpisode;
import nl.hkant.films.services.ContentReleaseInfoService;
import nl.hkant.films.services.ContentService;
import nl.hkant.films.web.dto.NewContentDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/series")
public class SeriesController {

    private final ContentService contentService;
    private final ContentReleaseInfoService contentReleaseInfoService;
    private final IEpisodeInfoService episodeInfoService;

    public SeriesController(
        ContentService contentService,
        ContentReleaseInfoService contentReleaseInfoService,
        IEpisodeInfoService episodeInfoService
    ) {
        this.contentService = contentService;
        this.contentReleaseInfoService = contentReleaseInfoService;
        this.episodeInfoService = episodeInfoService;
    }

    @PostMapping("/new")
    public NewContentDTO newSeries(@AuthenticationPrincipal User user, @RequestBody Series series) {
        series.setSeen(series.getRating() != 0);
        contentService.add(user, series);
        return new NewContentDTO(series.getId(), series.getTitle());
    }

    @PostMapping("/newFromExternal/{externalContentId}")
    public NewContentDTO newFromExternal(
        @AuthenticationPrincipal User user,
        @PathVariable int externalContentId,
        @RequestParam(defaultValue = "false", required = false) boolean interested
    ) {
        return contentService.createFromExternalInfo(user, externalContentId, interested, ContentType.SERIES)
            .map(series -> new NewContentDTO(series.getId(), series.getTitle()))
            .orElseThrow();
    }

    @GetMapping("/{id}/episodeInfo")
    public LastAndNextEpisode episodeInfo(@AuthenticationPrincipal User user, @PathVariable int id) {
        return Optional.of(contentService.findOrException(user, id))
            .filter(it -> it.getContentDetails() != null)
            .map(it -> episodeInfoService.getLastAndUpcomingEpisodes(it.getContentDetails().getExternalContentId()))
            .orElse(new LastAndNextEpisode(null, null));
    }

    @GetMapping("/upcoming")
    public List<UpcomingSeries> upcomingSeries(@AuthenticationPrincipal User user) {
        return contentReleaseInfoService.getWatchingOrAnticipatedSeriesLastAndNextEpisodes(user)
            .stream()
            .filter(it -> it.nextEpisode() != null)
            // use "z" as default string to put them behind the ones with an airDate
            .sorted(Comparator.comparing(o -> o.getNextEpisodeAirDate().orElse("z")))
            .map(UpcomingSeries::new)
            .toList();
    }

    public record UpcomingSeries(String title, EpisodeInfo episode) {
        public UpcomingSeries(ContentReleaseInfoService.SeriesAndEpisodes seriesAndEpisodes) {
            this(seriesAndEpisodes.series().title(), seriesAndEpisodes.nextEpisode());
        }
    }
}
