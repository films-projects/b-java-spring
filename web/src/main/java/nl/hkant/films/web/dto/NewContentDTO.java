package nl.hkant.films.web.dto;

public record NewContentDTO(int id, String title) {
}
