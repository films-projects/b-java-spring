package nl.hkant.films.web.controllers;

import nl.hkant.films.domain.User;
import nl.hkant.films.services.ContentReleaseInfoService;
import nl.hkant.films.services.ContentService;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/content")
public class ContentController {

    private final ContentService contentService;

    private final ContentReleaseInfoService contentReleaseInfoService;

    public ContentController(
        ContentService contentService,
        ContentReleaseInfoService contentReleaseInfoService
    ) {
        this.contentService = contentService;
        this.contentReleaseInfoService = contentReleaseInfoService;
    }

    @PostMapping("/{id}/update")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void update(
        @AuthenticationPrincipal User user,
        @PathVariable int id,
        @RequestParam Optional<String> title,
        @RequestParam Optional<Integer> year,
        @RequestParam Optional<Integer> rating,
        @RequestParam Optional<Boolean> specialInterest,
        @RequestParam Optional<Boolean> watching
    ) {
        contentService.update(user, id, title, year, rating, watching, specialInterest);
    }

    @PostMapping("/{id}/delete")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@AuthenticationPrincipal User user, @PathVariable int id) {
        contentService.delete(user, id);
    }

    @GetMapping("/upcoming")
    public UpcomingContent upcomingContent(
        @AuthenticationPrincipal User user,
        @RequestParam(required = false, defaultValue = "90") int days
    ) {
        var now = LocalDate.now();
        var seriesAndEpisodes = contentReleaseInfoService.getWatchingOrAnticipatedSeriesLastAndNextEpisodes(user);
        var seriesByNextEpisode = seriesAndEpisodes
            .stream()
            .collect(Collectors.groupingBy(it -> it.nextEpisode() != null));
        var withNextEpisode = seriesByNextEpisode.getOrDefault(true, Collections.emptyList())
            .stream()
            // use "z" as default string to put them behind the ones with an airDate
            .sorted(Comparator.comparing(o -> o.getNextEpisodeAirDate().orElse("z")))
            .toList();
        var noNextEpisode = seriesByNextEpisode.getOrDefault(false, Collections.emptyList())
            .stream()
            // use "z" as default string to put them behind the ones with an airDate
            .sorted(Comparator.comparing(o -> o.getLastEpisodeAirDate().orElse("z")))
            .toList()
            .reversed();

        var allSeries = new ArrayList<ContentReleaseInfoService.SeriesAndEpisodes>();
        allSeries.addAll(withNextEpisode);
        allSeries.addAll(noNextEpisode);

        return new UpcomingContent(
            allSeries,
            contentReleaseInfoService.upcomingMovies(user, now.plusDays(days)),
            contentReleaseInfoService.recentMovies(user, now.minusDays(days)).reversed()
        );
    }

    public record UpcomingContent(
        List<ContentReleaseInfoService.SeriesAndEpisodes> series,
        List<ContentReleaseInfoService.ContentRelease> upcomingMovies,
        List<ContentReleaseInfoService.ContentRelease> recentMovies
    ) {
    }
}
