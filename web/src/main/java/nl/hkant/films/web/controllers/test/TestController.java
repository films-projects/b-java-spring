package nl.hkant.films.web.controllers.test;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/test")
@Profile("dev")
@Controller
public class TestController {

    @RequestMapping("/error/500")
    public void error500() {
        throw new RuntimeException();
    }
}
