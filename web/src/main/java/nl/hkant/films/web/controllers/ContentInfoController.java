package nl.hkant.films.web.controllers;

import nl.hkant.films.domain.MovieDetails;
import nl.hkant.films.domain.SeriesDetails;
import nl.hkant.films.domain.User;
import nl.hkant.films.external.ContentType;
import nl.hkant.films.services.ContentInfoService;
import nl.hkant.films.services.repositories.ContentRepository;
import nl.hkant.films.web.dto.ContentInfoDTO;
import nl.hkant.films.web.dto.ContentInfoSearchResultDTO;
import nl.hkant.films.web.dto.FullContentInfoDTO;
import nl.hkant.films.web.dto.MovieContentInfoDTO;
import nl.hkant.films.web.dto.SeriesContentInfoDTO;
import nl.hkant.films.web.dto.SlideDTO;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/contentInfo")
public class ContentInfoController {

    private final ContentInfoService contentInfoService;
    private final ContentRepository contentRepository;

    public ContentInfoController(
        ContentInfoService contentInfoService,
        ContentRepository contentRepository
    ) {
        this.contentInfoService = contentInfoService;
        this.contentRepository = contentRepository;
    }

    @GetMapping("/search/{type}/{name}")
    public List<ContentInfoSearchResultDTO> findByTypeAndName(
        @AuthenticationPrincipal User user,
        @PathVariable ContentType type,
        @PathVariable String name
    ) {
        return contentInfoService.searchByTypeAndName(type, name)
            .stream()
            .map(it ->
                new ContentInfoSearchResultDTO(
                    it,
                    switch (type) {
                        case ContentType.FILM -> contentRepository.userHasAddedFilm(user.getId(), it.getExternalContentId());
                        case ContentType.SERIES -> contentRepository.userHasAddedSeries(user.getId(), it.getExternalContentId());
                    }
                )
            )
            .toList();
    }

    @PostMapping("/{contentId}/disconnect")
    public SlideDTO disconnect(@AuthenticationPrincipal User user, @PathVariable int contentId) {
        var content = contentInfoService.disconnect(user, contentId);
        return new SlideDTO(content);
    }

    @PostMapping("/{contentId}/connect/{externalId}")
    public ContentInfoDTO connect(
        @AuthenticationPrincipal User user,
        @PathVariable int contentId,
        @PathVariable String externalId
    ) {
        var content = contentInfoService.storeInfoForContent(user, contentId, externalId);
        // TODO if null return error
        return switch (content.getContentDetails()) {
            case SeriesDetails info -> new SeriesContentInfoDTO(content, info);
            case MovieDetails info -> new MovieContentInfoDTO(content, info);
            default -> new ContentInfoDTO(content, null);
        };
    }

    @PostMapping("/{id}/refresh")
    public ContentInfoDTO refresh(@AuthenticationPrincipal User user, @PathVariable int id) {
        var content = contentInfoService.refresh(user, id);
        return switch (content.getContentDetails()) {
            case SeriesDetails info -> new SeriesContentInfoDTO(content, info);
            case MovieDetails info -> new MovieContentInfoDTO(content, info);
            default -> new ContentInfoDTO(content, null);
        };
    }

    @GetMapping("/{contentId}/full")
    public FullContentInfoDTO full(@AuthenticationPrincipal User user, @PathVariable int contentId) {
        return new FullContentInfoDTO(contentInfoService.getFullContentInfo(user, contentId));
    }
}
