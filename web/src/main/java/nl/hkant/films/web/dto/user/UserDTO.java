package nl.hkant.films.web.dto.user;

public record UserDTO(boolean userIsCreator) {
}
