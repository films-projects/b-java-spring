package nl.hkant.films.web.dto;

import java.util.List;
import java.util.Map;

public record SavedQuery(String query, Map<String, List<String>> params) {
}
