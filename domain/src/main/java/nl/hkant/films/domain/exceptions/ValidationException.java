package nl.hkant.films.domain.exceptions;

public class ValidationException extends RuntimeException {

	public ValidationException() {

	}

	public ValidationException(String s) {
		super(s);
	}
}
