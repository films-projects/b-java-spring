package nl.hkant.films.domain;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.Getter;
import lombok.Setter;
import nl.hkant.films.external.ContentType;

import java.time.LocalDate;

@Setter
@Getter
@Entity
@DiscriminatorValue("SERIES")
@PrimaryKeyJoinColumn(name = "id")
public final class SeriesDetails extends ContentDetails<SeriesDetails> {

    private LocalDate firstAirDate;
    private LocalDate lastAirDate;
    private int episodeRuntime;
    private int numSeasons;
    private int numEpisodes;
    @Enumerated(EnumType.STRING)
    private SeriesStatus status;

    public SeriesDetails() {

    }

    @Override
    public LocalDate getAirDate() {
        return firstAirDate;
    }

    @Override
    public void update(SeriesDetails info) {
        posterPath = info.posterPath;
        backdropPath = info.backdropPath;
        overview = info.overview;
        imdbId = info.imdbId;
        firstAirDate = info.firstAirDate;
        lastAirDate = info.lastAirDate;
        numEpisodes = info.numEpisodes;
        numSeasons = info.numSeasons;
        episodeRuntime = info.episodeRuntime;
        status = info.status;
        imdbRating = info.imdbRating;
        watchOptions.clear();
        watchOptions.addAll(info.getWatchOptions());
    }

    public ContentType contentType() {
        return ContentType.SERIES;
    }
}
