package nl.hkant.films.domain;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "persistent_logins")
public class PersistentLogin {

	@Column(length = 64, nullable = false)
	private String username;
	@Id
	@Column(length = 64, nullable = false)
	private String series;
	@Column(length = 64, nullable = false)
	private String token;
	@Column(columnDefinition = "timestamp", nullable = false)
	private String last_used;

}
