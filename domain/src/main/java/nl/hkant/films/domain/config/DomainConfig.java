package nl.hkant.films.domain.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@EntityScan("nl.hkant.films.domain")
public class DomainConfig {

}
