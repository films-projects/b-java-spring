package nl.hkant.films.domain;

import lombok.Getter;

@Getter
public enum SeriesStatus {

    ONGOING("Ongoing"),
    UPCOMING("Upcoming"),
    ENDED("Ended"),
    CANCELLED("Cancelled"),
    UNKNOWN("Unknown");

    private final String friendlyName;

    SeriesStatus(String friendlyName) {
        this.friendlyName = friendlyName;
    }
}
