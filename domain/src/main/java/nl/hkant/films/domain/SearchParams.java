package nl.hkant.films.domain;

import lombok.Data;

import java.util.List;

@Data
public class SearchParams {

    private String title;
    private Integer yearFrom;
    private Integer yearTo;
    private Boolean anticipated;
    private List<Integer> myRating;
    private List<StreamingPlatform> watch;
    private String type;
    private Boolean watching;
    private Double minRating;
    private SeriesStatus status;
    private Boolean released;
}
