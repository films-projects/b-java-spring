package nl.hkant.films.domain;

public final class ConfigKey {
	public static final String TMDB_KEY = "tmdb.api.key";

	public static final String OMDB_API_TOKEN = "omdb.token";

	public static final String REFRESH_EXPIRATION_LIMIT = "scheduled.refresh.day.expiration";

	/**
	 * list of enum names of watch options, e.g. [AMAZON_PRIME, NETFLIX]
	 */
	public static final String USABLE_WATCH_OPTIONS = "watch.options.usable";

}
