package nl.hkant.films.domain;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import nl.hkant.films.external.ContentType;

@Setter
@Getter
@Entity
@DiscriminatorValue("FILM")
@Table(name = "Content")
public class Film extends Content {

    @Transient
    public MovieDetails getMovieDetails() {
        return (MovieDetails) contentDetails;
    }

    public ContentType contentType() {
        return ContentType.FILM;
    }
}
