package nl.hkant.films.domain;

import jakarta.persistence.CollectionTable;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.DiscriminatorType;
import jakarta.persistence.ElementCollection;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import nl.hkant.films.external.ContentType;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Setter
@Getter
@Entity
@Table(name = "ContentDetails")
@DiscriminatorColumn(name = "type", discriminatorType = DiscriminatorType.STRING)
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class ContentDetails<T extends ContentDetails<T>> {

    public static final String IMG_URL = "http://image.tmdb.org/t/p/w300/";
    public static final String BACKDROP_URL = "https://image.tmdb.org/t/p/original";
    public static final String IMDB_URL = "https://www.imdb.com/title/";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;

    @OneToMany(mappedBy = "contentDetails", fetch = FetchType.EAGER)
    protected List<Content> content;

    protected int externalContentId;
    protected String backdropPath;
    protected String posterPath;
    @Column(length = 2000)
    protected String overview;
    @Column(length = 15)
    protected String imdbId;
    protected Double imdbRating;
    @Column
    protected LocalDate lastRefresh;
    @ElementCollection()
    @CollectionTable(name = "content_watch_options", joinColumns = @JoinColumn(name = "content_id"))
    @Column(name = "watch_option")
    @Enumerated(EnumType.STRING)
    protected Set<StreamingPlatform> watchOptions;

    @Transient
    private String externalContentTitle;

    public Optional<String> getPosterUrl() {
        return Optional.ofNullable(posterPath).map(it -> IMG_URL + it);
    }

    public LocalDate getAirDate() {
        return null;
    }

    public abstract void update(T info);

    public void updateLastRefresh() {
        lastRefresh = LocalDate.now();
    }

    public abstract ContentType contentType();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ContentDetails<?> that = (ContentDetails<?>) o;

        if (id != that.id) {
            return false;
        }
        return externalContentId == that.externalContentId;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + externalContentId;
        return result;
    }
}
