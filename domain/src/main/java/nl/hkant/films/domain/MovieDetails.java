package nl.hkant.films.domain;

import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.PrimaryKeyJoinColumn;
import lombok.Getter;
import lombok.Setter;
import nl.hkant.films.external.ContentType;

import java.time.LocalDate;

@Setter
@Getter
@Entity
@DiscriminatorValue("FILM")
@PrimaryKeyJoinColumn(name = "id")
public final class MovieDetails extends ContentDetails<MovieDetails> {

    private LocalDate releaseDate;
    private int runtime;

    public MovieDetails() {

    }

    @Override
    public void update(MovieDetails info) {
        externalContentId = info.externalContentId;
        posterPath = info.posterPath;
        overview = info.overview;
        imdbId = info.imdbId;
        imdbRating = info.imdbRating;
        backdropPath = info.backdropPath;
        releaseDate = info.releaseDate;
        runtime = info.runtime;
        watchOptions.clear();
        watchOptions.addAll(info.getWatchOptions());
    }

    @Override
    public LocalDate getAirDate() {
        return releaseDate;
    }

    public ContentType contentType() {
        return ContentType.FILM;
    }
}
