package nl.hkant.films.domain;

public enum StreamingPlatform {
    NETFLIX,
    AMAZON_PRIME,
    APPLE_TV,
    HBO_MAX,
    SKY_SHOWTIME,
}
