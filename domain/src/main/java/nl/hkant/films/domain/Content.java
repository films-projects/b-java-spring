package nl.hkant.films.domain;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorColumn;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Inheritance;
import jakarta.persistence.InheritanceType;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import nl.hkant.films.external.ContentType;

@Setter
@Getter
@Entity
@Table(name = "Content")
@DiscriminatorColumn(name = "type")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Content {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected int id;
    protected String title;
    protected int year;
    protected boolean seen = false;
    @Column(nullable = false)
    protected int rating = 0;
    protected boolean anticipated = false;
    protected int userId;

    @ManyToOne(cascade = {CascadeType.MERGE, CascadeType.PERSIST, CascadeType.REFRESH}, fetch = FetchType.EAGER)
    @JoinColumn(name = "contentDetailsId")
    protected ContentDetails<?> contentDetails;

    public void copyFrom(Content other) {
        setTitle(other.getTitle());
        setYear(other.getYear());
    }

    public abstract ContentType contentType();

    @Override
    public String toString() {
        return "Content [id=" + id + ", title=" + title + ", year=" + year + ", seen=" + seen
            + ", rating=" + rating + "]";
    }
}
