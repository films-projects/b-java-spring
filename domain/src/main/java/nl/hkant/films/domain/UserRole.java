package nl.hkant.films.domain;

import org.springframework.security.core.GrantedAuthority;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.persistence.UniqueConstraint;

@Entity
@Table(uniqueConstraints = @UniqueConstraint(columnNames = {"role", "userId"}))
public class UserRole implements GrantedAuthority {

	private static final long serialVersionUID = 3499278278542737590L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer id;
	@Column(length = 45, nullable = false)
	@Enumerated(EnumType.STRING)
	private Role role;

	public UserRole() {
	}

	public UserRole(Role role) {
		this.role = role;
	}

	public Integer getUserRoleId() {
		return id;
	}

	public void setUserRoleId(Integer userRoleId) {
		this.id = userRoleId;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public String toString() {
		// don't change it, it's used on the sequence viewer jsp
		return role.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((role == null) ? 0 : role.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		UserRole other = (UserRole) obj;
		if (role != other.role) {
			return false;
		}
		return true;
	}

	public String getAuthority() {
		return getRole().toString();
	}

}
