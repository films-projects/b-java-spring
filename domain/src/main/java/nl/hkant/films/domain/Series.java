package nl.hkant.films.domain;

import jakarta.persistence.Column;
import jakarta.persistence.DiscriminatorValue;
import jakarta.persistence.Entity;
import jakarta.persistence.SecondaryTable;
import jakarta.persistence.Table;
import jakarta.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import nl.hkant.films.external.ContentType;

@Setter
@Getter
@Entity
@DiscriminatorValue("SERIES")
@SecondaryTable(name = "Series")
@Table(name = "Content")
public class Series extends Content {

    @Column(table = "Series")
    private boolean watching;

    @Override
    public void copyFrom(Content other) {
        super.copyFrom(other);
        this.watching = ((Series) other).isWatching();
    }

    @Transient
    public SeriesDetails getSeriesDetails() {
        return (SeriesDetails) contentDetails;
    }

    public ContentType contentType() {
        return ContentType.SERIES;
    }
}
