package nl.hkant.films.domain;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Entity
@Table(name = "SearchFavorite")
@Getter
@Setter
public class StoredSearchQuery {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    int id;
    int userId;
    String query;

    public Map<String, List<String>> params() {
        var b = Arrays.stream(query.split("&"))
            .map(q -> q.split("="))
            .collect(Collectors.groupingBy(param -> param[0]));
        return b.entrySet().stream()
            .collect(
                Collectors.toMap(
                    Map.Entry::getKey,
                    entry -> entry.getValue().stream().map(e -> e[1]).toList()
                ));
    }
}
