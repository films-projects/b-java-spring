package nl.hkant.films.domain;

import lombok.Getter;
import lombok.Setter;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

@Getter
@Setter
@Entity
public class Configuration {

	@Id
	private String configKey;
	private String configValue;
}
