package nl.hkant.films.domain;

public enum Role {
	ROLE_ADMIN, ROLE_USER;
}
