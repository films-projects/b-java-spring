# films-spring

Original films project, using

-   Maven
-   Java 23
-   Spring Boot 3.4
    -   using Spring Data JPA repositories and Specification
    -   using `@PreAuthorize` for securing endpoints
-   Hibernate for database access
-   [Films JS frontend](https://gitlab.com/films/f-react-js)
-   Docker for deployment

## Deployment

This project can be deployed using the following service in a `docker-compose.yml`:

```
services:
  films-spring:
    mem_limit: 400m
    image: registry.gitlab.com/films-projects/b-java-spring
    container_name: films-spring
    depends_on:
      - mysql-films
    environment:
      - SPRING_DATASOURCE_URL=jdbc:mysql://mysql-films:3306/films?serverTimezone=UTC
      - SPRING_DATASOURCE_USERNAME=root
      - SPRING_DATASOURCE_PASSWORD=passwordFromMysqlFilms
    ports:
      - "8078:8080"
```

which will expose this application on port 8078, and assumes a MySQL database running in another container called
`mysql-films`.

## Running the application

You can find pointers on running the application in [setup.md](setup.md).

## Database migration

When the app starts, it applies the migrations automatically to the database, and the schema may be completely empty
upon first launch.
