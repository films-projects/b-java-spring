@echo off
ECHO Logging in to registry..
docker login registry.gitlab.com/films-projects/b-java-spring

SET /p tag="Give docker tag: "

ECHO Building docker image
docker build -f Dockerfile -t registry.gitlab.com/films-projects/b-java-spring ../

ECHO Tagging..
docker tag registry.gitlab.com/films-projects/b-java-spring registry.gitlab.com/films-projects/b-java-spring:%tag%

ECHO Pushing tag..
docker push registry.gitlab.com/films-projects/b-java-spring:%tag%

@REM ECHO Pushing latest..
@REM docker push registry.gitlab.com/films-projects/b-java-spring

